package com.metamorphosys.insureconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmsGranularApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmsGranularApiApplication.class, args);
	}

}
